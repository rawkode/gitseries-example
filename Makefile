influxdb:
	docker-compose up -d influxdb

setup: influxdb
	sleep 5
	docker-compose exec influxdb influx setup -f -o org -u user -p password123 -t token -b gitseries -r 0
	docker-compose exec influxdb influx pkg --file /pkger.yaml --force=true

.PHONY: influxdb setup
